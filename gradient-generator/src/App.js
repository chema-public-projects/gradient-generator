import 'rc-color-picker/assets/index.css';
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ColorPicker from 'rc-color-picker'
import { Button, Container, Row, Col } from 'react-bootstrap';

const styles = {
  button: {
    margin: 5
  },
  mainContainer: {
    padding: 10,
    marginTop: 15,
    background: "#1b1c1c",
    color: "#a0a0a0"
  },
  codePreview: {
    padding: 10,
    marginTop: 20,
    border: "solid 1px #FFF",
    borderRadius: 12,
  },
  section: {
    marginTop: 20
  }
}

export default class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      templates: [],
      templateList: [],
      newTemplate: {name: "", content: "", author: ""}, 
      gradientType: "linear",//o gradient
      buttonDirectionValue: "to bottom",
      xDirection: "to bottom",
      yDirection: "",
      firstColor: "#16b1de",
      secondColor: "#d6f0f9",
      backgroundStyle: {
        oldBrowsers: "#16b1de",
        webkit: "-webkit-linear-gradient(top, #16b1de, #d6f0f9)",
        o: "-o-linear-gradient(top, #16b1de, #d6f0f9)",
        moz: "-moz-linear-gradient(top, #16b1de, #d6f0f9)",
        newestBrowsers: "linear-gradient(to bottom, #16b1de, #d6f0f9"
      },
      styleColourPreview: {
        minHeight: 300,
        borderRadius: 12,
        backgroundImage: "linear-gradient(to bottom, #16b1de, #d6f0f9"
      },
    }
  }

  /**
   * Actualiza los estilos y el estilo de la muestra
   * @param {*} color Indica cual color se va a actualizar
   * - firstColor
   * - secondColor
   * @param {*} code El codigo de color en hexa
   */
  async updateColor(color, code) {
    let colors = {}
    colors[color] = code
    await this.setState(colors)
    let styles = this.getStyles()

    this.setState({
      backgroundStyle: styles.backgroundStyle,
      styleColourPreview: styles.styleColourPreview
    })
  }

  /**
   * Intenta guardar los estilos como una plantilla.
   * TODO: Implementar funcionalidad para guardar en una base de datos
   */
  saveTemplate(){
    let templateName = prompt("Give it a template name:", "")
    let author = prompt("What is your name?", "")


    if (templateName && author) {
      alert(`Ok ${author}, the template "${templateName}" will be saved. Check the console, please.`)
      
      let newTemplate = {
        name: templateName, 
        author: author,
        content: `
          background: "${this.state.backgroundStyle.oldBrowsers}";
          background: "${this.state.backgroundStyle.webkit}";
          background: "${this.state.backgroundStyle.o}";
          background: "${this.state.backgroundStyle.moz}";
          background: "${this.state.backgroundStyle.newestBrowsers}";`
      }
      
      let {templates} = this.state
      templates.push(newTemplate)
      this.setState({
        templates: templates
      })

      console.log(templates)
      this.loadSavedTemplates()
    }
  }


  /**
   * Cambia la direccion del gradient
   * @param {*} direction 
   * - to bottom
   * - to top
   * - to left
   * - to right
   * - to bottom left
   * - to bottom right
   * - to top left
   * - to top right
   */
  async changeDirection(direction) {
    await this.setState({ buttonDirectionValue: direction })
    let styles = this.getStyles()

    this.setState({
      backgroundStyle: styles.backgroundStyle,
      styleColourPreview: styles.styleColourPreview
    })
  }

  /**
   * Devuelve un objeto con dos propiedades:
   * old: la direccion para navegadores antiguos
   * newest: la direccion para navegadores actuales
   */
  getDirection() {
    let direction = {
      old: "top",
      newest: "to bottom"
    }

    let { buttonDirectionValue } = this.state
    direction.newest = buttonDirectionValue

    switch (buttonDirectionValue) {
      case "to bottom"://hacia arriba
        direction.old = "top"
        break
      case "to top"://hacia abajo
        direction.old = "bottom"
        break
      case "to left"://hacia la izquierda
        direction.old = "right"
        break
      case "to right"://hacia la derecha
        direction.old = "left"
        break
      case "to bottom left"://diagonal abajo izquierda
        direction.old = "up right"
        break
      case "to bottom right"://diagonal abajo izquierda
        direction.old = "up left"
        break
      case "to top left"://diagonal abajo izquierda
        direction.old = "bottom right"
        break
      case "to top right"://diagonal abajo izquierda
        direction.old = "bottom left"
        break
      default:
        direction.old = "top"
        break
    }

    return direction
  }

  /**
   * Devuelve un objeto de estilos de acuerdo a los parámetros (del state)
   * que indiquen si es un linear o radial, así como la dirección y el color seleccionados:
   * - backgroundStyle
   * - styleColourPreview
   */
  getStyles() {
    let direction = this.getDirection()
    let backgroundStyle = {}
    let styleColourPreview = {}

    if (this.state.gradientType === "linear") {//
      backgroundStyle = {//
        oldBrowsers: `${this.state.firstColor}`,
        webkit: `-webkit-linear-gradient(${direction.old}, ${this.state.firstColor}, ${this.state.secondColor})`,
        o: `-o-linear-gradient(${direction.old}, ${this.state.firstColor}, ${this.state.secondColor})`,
        moz: `-moz-linear-gradient(${direction.old}, ${this.state.firstColor}, ${this.state.secondColor})`,
        newestBrowsers: `linear-gradient(${direction.newest}, ${this.state.firstColor}, ${this.state.secondColor})`,
      }
      styleColourPreview = {//
        minHeight: 300,
        borderRadius: 12,
        backgroundImage: `linear-gradient(${direction.newest}, ${this.state.firstColor}, ${this.state.secondColor})`
      }
    }
    else {//
      //
      backgroundStyle = {
        oldBrowsers: `${this.state.firstColor}`,
        webkit: `-webkit-radial-gradient(${this.state.firstColor}, ${this.state.secondColor})`,
        o: `-o-radial-gradient(${this.state.firstColor}, ${this.state.secondColor})`,
        moz: `-moz-radial-gradient(${this.state.firstColor}, ${this.state.secondColor})`,
        newestBrowsers: `radial-gradient(${this.state.firstColor}, ${this.state.secondColor})`,
      }
      //
      styleColourPreview = {
        minHeight: 300,
        borderRadius: 12,
        backgroundImage: `radial-gradient(${this.state.firstColor}, ${this.state.secondColor})`
      }
    }

    //Estilos
    let styles = {
      backgroundStyle: backgroundStyle,
      styleColourPreview: styleColourPreview
    }

    return styles
  }

  /**
   * Devuelve un array con la lista de templates
   */
  loadSavedTemplates(){
    let {templates} = this.state

    let listTemplates = []

    templates.map((item, index) => {
      listTemplates.push(
        <p key={index.toString()}>Template "{item.name}" created by {item.author}</p>
      )
    })

    this.setState({
      templateList: listTemplates
    })
  }

  /**
   * Cambia en el state el tipo de grandiente
   * @param {*} type TIpo de gradiente:
   * - linear
   * - radial
   */
  async changeGradientType(type) {
    await this.setState({ gradientType: type })
    let styles = this.getStyles()

    this.setState({
      backgroundStyle: styles.backgroundStyle,
      styleColourPreview: styles.styleColourPreview
    })
  }

  /**
   * Renderiza los elementos gráficos
   */
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Row>
          <Col xs={12}><h4>CSS GRADIENT</h4></Col>
          <Col xs={12}>Gradient Generator for Linear and Radial CSS Color Gradients</Col>
        </Row>
        <Row>
          <Col className="parameters">
            <Col xs={12} style={styles.section}>
              <Col xs={12}>STYLE:</Col>
              <Col xs={12}>
                <Button variant="dark" style={styles.button} onClick={() => { this.changeGradientType("linear") }}>Linear</Button>
                <Button variant="dark" style={styles.button} onClick={() => { this.changeGradientType("radial") }}>Radial</Button>
              </Col>
            </Col>
            <Col xs={12} style={styles.section}>
              <Col xs={12}>DIRECTION:</Col>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to bottom") }}>Top</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to bottom left") }}>Top right</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to left") }}>Right</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to top left") }}>Bottom right</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to top") }}>Bottom</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to top right") }}>Bottom left</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to right") }}>Left</Button>
              <Button variant="dark" style={styles.button} onClick={() => { this.changeDirection("to bottom right") }}>Top left</Button>
            </Col>
            <Col xs={12} style={styles.section}>
              <Col xs={12}>COLORS:</Col>

              <Col xs={12}>
                <ColorPicker
                  animation="slide-up"
                  color={this.state.firstColor}
                  onChange={event => {
                    this.updateColor("firstColor", event.color)
                  }} /> {this.state.firstColor}
              </Col>
              <Col xs={12}>
                <ColorPicker
                  animation="slide-up"
                  color={this.state.secondColor}
                  onChange={event => {
                    this.updateColor("secondColor", event.color)
                  }} /> {this.state.secondColor}
              </Col>

            </Col>
            <Col xs={12} style={styles.section}>
              <Col xs={12}>FORMAT:</Col>
              <Button variant="dark" style={styles.button}>Hex</Button>
              <Button variant="dark" style={styles.button}>RGB</Button>
            </Col>
            <Col xs={12} style={styles.section}>
              <Button variant="danger" style={styles.button} onClick={this.saveTemplate.bind(this)}>SAVE AS TEMPLATE</Button>
            </Col>
          </Col>


          <Col className="result">
            <Col xs={12}>
              <div id={"preview"} style={this.state.styleColourPreview}>
              </div>
            </Col>
            <Col xs={12} style={styles.codePreview}>
              <code style={{color: "#FFF"}}>
                <p>background: {this.state.backgroundStyle.oldBrowsers}<spam style={{color: "#F68"}}>/*old browsers*/</spam></p>
                <p>background: {this.state.backgroundStyle.webkit}</p>
                <p>background: {this.state.backgroundStyle.o}</p>
                <p>background: {this.state.backgroundStyle.moz}</p>
                <p>background: {this.state.backgroundStyle.newestBrowsers}</p>
              </code>
            </Col>
            <Col xs={12} style={styles.codePreview}>
              {
                this.state.templateList.length > 0 ? this.state.templateList : null
              }
            </Col>
          </Col>
        </Row>
      </Container>
    )
  }

}


